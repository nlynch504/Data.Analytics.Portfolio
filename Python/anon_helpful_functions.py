# required libraries
import requests
import pandas as pd
import chromedriver_binary
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import selenium.webdriver as webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.select import Select
import os, shutil, glob
import mysql.connector as mysql
import numpy as np
from datetime import date, timedelta
import datetime
import time

# web scrapping libraries
import lxml 
import pyperclip
import requests 
import bs4
from lxml import html


# email libraries
import smtplib
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders 
import time
import json


# take concatenated values that are in a dataframe, convert to list, and copy to clipboard for sql for concat values greater than excels 5k/50k cell limit
list_final = df_values['Final list'].tolist()
str_final = list_final[1:-1]
pyperclip.copy(str_final)
pyperclip.paste()

#google sheets read pickle file
os.chdir(os.path.expanduser('~'))

import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
   
SCOPES = 'https://www.googleapis.com/auth/spreadsheets'
    
creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
if os.path.exists('token.pickle'):
    with open('token.pickle', 'rb') as token:
        creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
if not creds or not creds.valid:
    if creds and creds.expired and creds.refresh_token:
        creds.refresh(Request())
    else:
        flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
        creds = flow.run_local_server()
        # Save the credentials for the next run
    with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

# set service variabele
service = build('sheets', 'v4', credentials=creds)
#                 ,cache_discovery=False)


# yes or no interface
def yes_or_no(question):
    reply = str(input(question+' (y/n): ')).lower().strip()
    if reply[0] == 'y':
        return True
    if reply[0] == 'n':
        return False
    else:
        return yes_or_no("Uhhhh... please enter ")
yes_or_no('Do you want to continue?')

# get final working directory for final data location
final_wd = str(os.getcwd())

# chg directory to downloads for any users
os.chdir(os.path.expanduser('~/Downloads'))

# regex search for files
# get working directory and assign filepath for csv files
wd = os.getcwd()
wd = wd + '/*.csv'
file_path = sorted(glob.glob(wd))

# assign filepath for 1st csv file
file_path_1 = str(file_path[0])

# transform dataframe to list for google sheets

test_g_data = g_data.copy()
df_column_names = list(test_g_data)
df_values = test_g_data.values.tolist()
values = [df_column_names] + df_values

# specific spreadsheet and range 
spreadsheet_id='google sheets id'
range_names = "'sheet name'!A:I"

# clear ranges
request = service.spreadsheets().values().clear(spreadsheetId=spreadsheet_id, range=range_names)
response = request.execute()

## fill google sheets
# specify input format
value_input_option = 'USER_ENTERED'


value_range_body = {
    'majorDimension': 'ROWS',
    'values':values
}

# specify how rows will be inserted
insert_data_option = 'INSERT_ROWS'

# append new values
request = service.spreadsheets().values().append(
    spreadsheetId = spreadsheet_id, range=range_names,
    valueInputOption=value_input_option
    ,insertDataOption=insert_data_option
    , body=value_range_body)

response = request.execute()

# get current week number
w = date.today()
w = w.isocalendar()[1]


#write multiplte data frames to excel
if start_month != end_month:
    with pd.ExcelWriter('week ' + str(w) + ' lgn_rev.xlsx') as writer:
        df_lgn_report.to_excel(writer, sheet_name = 'raw_data',index=False)
        hc_summary.to_excel(writer, sheet_name = 'hcom_states',index=False)
        all_summary.to_excel(writer, sheet_name= 'all_states',index=False)

# import df 1
df_merge = pd.read_csv(filepath)
df_merge.set_index(['index_1','index_2'],inplace=True)

# import df 2
df_2 = pd.read_csv(filepath)
df_2.set_index(['index_1','index_2'],inplace=True)
        
# merge data frames

df_merge = df_merge.join(df_2,on = ['index_1','index_2'],rsuffix='_df_2')

'''
use np.where to reduce columns to final format
https://stackoverflow.com/questions/39903090/efficiently-replace-values-from-a-column-to-another-column-pandas-dataframe
'''

df_merge['Clicks'] = np.where(df_merge['Clicks'] == 'NA',df_merge['App Clicks'],df_merge['Clicks'])
df_merge['commission'] = np.where(df_merge['commission'] == 'NA',df_merge['Spent'],df_merge['commission'])

# app_2
df_merge['Clicks'] = np.where(df_merge['Clicks'].isnull(),df_merge['Clicks_app_2'],df_merge['Clicks'])
df_merge['revenue'] = np.where(df_merge['revenue'] == 'NA',df_merge['Revenue'],df_merge['revenue'])
df_merge['commission'] = np.where(df_merge['commission'].isnull(),df_merge['Costs'],df_merge['commission'])

# drop un-needed columns
df_merge.drop(inplace=True,columns=['App Clicks','Spent','Clicks_app_2','Revenue','Costs'])

# drop indices for final table
df_merge.reset_index(inplace=True)

# create final data frame
df_final_form = df_merge.copy()

df_final_form['report_date'] = pd.to_datetime(df_final_form['report_date']).dt.strftime('%m/%d/%Y')

# rearrange columns
cols = df_final_form.columns.tolist()
cols = cols[2:4] + [cols[1]] + cols[4:] + [cols[0]]

df_final_form = df_final_form[cols]

# replace na values with 0
df_final_form.fillna(inplace=True,value=0)

# create summary dataframe
df_summary = df_final_form.groupby(['Job Board'],as_index=False)['Clicks','comission'].sum()