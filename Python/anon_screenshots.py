# required libraries
import requests
import pandas as pd
import chromedriver_binary
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import selenium.webdriver as webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.select import Select
import os, shutil, glob
import mysql.connector as mysql
import numpy as np
from datetime import date, timedelta
import datetime
import time

# web scrapping libraries
import lxml 
import pyperclip
import requests 
import bs4
from lxml import html


# email libraries
import smtplib
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders 
import time
import json

# get final working directory for final data location
final_wd = str(os.getcwd())

# get current week number
d = date.today() - timedelta(1)
print(d)

# launch googlesheet where screenshots will be taken
driver = webdriver.Chrome()
driver.get('https://docs.google.com/spreadsheets/d/#spreadsheet_id#/edit#gid=2066007120')
wait = WebDriverWait(driver, 60)

# have user enter in username
print('enter in username')
driver.find_element_by_id('identifierId').send_keys(input())
driver.find_element_by_xpath('//*[@id="identifierNext"]/content/span').click()
time.sleep(5)

# have user enter in pw
print('enter in password')
driver.find_element_by_name('password').send_keys(input())
driver.find_element_by_xpath('//*[@id="passwordNext"]/content/span').click()


#
yes_or_no('Press y to continue after entering in user info')
# now that we have the preliminary stuff out of the way time to get that image :D

driver.find_element_by_xpath('//*[@class="docs-sheet-tab-name"][text()="Revenue"]').click()

# make window full screen 
driver.fullscreen_window()

#change view to 75%
yes_or_no('Press y once zoom has been set to 75%')

#change window size
driver.set_window_size(1600, 1060)

# wailt for web element to appear
element = wait.until(EC.presence_of_element_located((By.XPATH,'//*[@class="goog-inline-block grid4-inner-container"][@style="width: 1497px; height: 737px;"]')))
element = driver.find_element_by_xpath('//*[@class="goog-inline-block grid4-inner-container"][@style="width: 1497px; height: 737px;"]')

# set location and size for screenshot
location = element.location
size = element.size

# uses PIL library to open image in memory


im = driver.get_screenshot_as_png()
im = Image.open(BytesIO(im))


left = location['x']
top = location['y']
right = location['x'] + size['width']
bottom = location['y'] + size['height']

# defines crop points
im = im.crop((left, top, right, bottom)) 
# saves new cropped image
im.save(str(d) + ' Revenue.png') 

# click on costs tab 
driver.find_element_by_xpath('//*[@class="docs-sheet-tab-name"][text()="Costs"]').click()

driver.set_window_size(672,1014)

# wait for screenshot element to load
element = wait.until(EC.presence_of_element_located((By.XPATH,'//*[@class="goog-inline-block grid4-inner-container"][@style="width: 569px; height: 693px;"]')))
element = driver.find_element_by_xpath('//*[@class="goog-inline-block grid4-inner-container"][@style="width: 569px; height: 693px;"]')

# make cost screenshot
location = element.location
size = element.size

im = driver.get_screenshot_as_png()
im = Image.open(BytesIO(im))


left = location['x']
top = location['y']
right = location['x'] + size['width']
bottom = location['y'] + size['height']

# defines crop points
im = im.crop((left, top, right, bottom)) 
# saves new cropped image
im.save(str(d) + ' Cost.png') 

# get file path for automated emails
wd = os.getcwd()
wd = wd + '/' + str(d) + '*png'
file_path = sorted(glob.glob(wd))
print(file_path)

# email recipients 
fromaddr = 'userid@gmail.com'

toaddr = ['email1@gmail.com','email2@gmail.com','email3@gmail.com']

# start mime instance
msg = MIMEMultipart()

# storing recipients info
msg['From'] = fromaddr
msg['To'] = ', '.join(toaddr)

# storing subject
msg['Subject'] = str(d) + ' daily report'

# create body of email var
body = '''<pre>
Hello all,

See <a href = 'https://docs.google.com/spreadsheets/d/#spreadsheet_id#/edit?usp=sharing'>link</a> for daily report. Screenshots are attached.

Let me know if you have any questions or concerns.

Best regards,

Nicholas 
</pre>
'''

# attach body var to mesaage
msg.attach(MIMEText(body, 'html'))


# set attachment variable for automated emails
attachments = file_path

# attach multiplle screenshots 
for filename in attachments:
    f = filename
    part = MIMEBase('application', "octet-stream")
    part.set_payload( open(f,"rb").read() )
    encoders.encode_base64(part)
    part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(f))
    msg.attach(part)
    
# create sessions
s = smtplib.SMTP('smtp.gmail.com', #port number#)

# start tls security protocol
s.starttls()

# authenication and send email
s.login(fromaddr,'user pw')
text = msg.as_string()
s.sendmail(fromaddr,toaddr,text)