# required libraries
import requests
import pandas as pd
import chromedriver_binary
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import selenium.webdriver as webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.select import Select
import os, shutil, glob
import mysql.connector as mysql
import numpy as np
from datetime import date, timedelta
import datetime
import time

# web scrapping libraries
import lxml 
import pyperclip
import requests 
import bs4
from lxml import html


''' wait for chrome download to finish '''
def every_downloads_chrome(driver):
    if not driver.current_url.startswith("chrome://downloads"):
        driver.get("chrome://downloads/")
    return driver.execute_script("""
        var items = downloads.Manager.get().items_;
        if (items.every(e => e.state === "COMPLETE"))
            return items.map(e => e.file_url);
        """)

''' set wait variable and expected condition and wait for login '''
wait = WebDriverWait(driver, 60)
element = wait.until(EC.presence_of_element_located((By.ID,'user_session_email')))


''' waits for all the files to be completed and returns the paths '''
# paths = 
WebDriverWait(driver, 120, 1).until(every_downloads_chrome)
# print(paths)

''' weekday functions and writes variable to list for weekend '''
# get date variables for past 3 days if current weekday = 0 / Monday
t = date.today()
weekday = t.weekday()
print(weekday)

if weekday == 0:
# get friday date if current day = Monday / 0 
    Friday = t - timedelta(days=3)
    dates = [Friday]
    for i in range(1,3):
        dates.append(Friday + timedelta(days=i))
    dates = [str(i) for i in dates]
    print(dates)
else: 
    d = date.today() - timedelta(days=1)
    d = str(d) 
    
''' concat date values to dataframes and forward fill na values '''
if weekday == 0:
#     create date variables to cocnat
    series_fri = pd.Series(dates[0])
    series_sat = pd.Series(dates[1])
    series_sun = pd.Series(dates[2])    
#     concat values
    concat_fri = pd.concat([summary_Fri,series_fri],axis=1)
    concat_sat = pd.concat([summary_Sat,series_sat],axis=1)
    concat_sun = pd.concat([summary_Sun,series_sun],axis=1)
# rename columns
    concat_fri.rename(columns={0 : 'Date'},inplace=True)
    concat_fri.fillna(method = 'ffill',inplace=True)
    concat_sat.rename(columns={0 : 'Date'},inplace=True)
    concat_sat.fillna(method = 'ffill',inplace=True)
    concat_sun.rename(columns={0 : 'Date'},inplace=True)
    concat_sun.fillna(method = 'ffill',inplace=True)
else: 
    series_weekday = pd.Series(d)
    concat_weekday = pd.concat([summary_test,series_weekday],axis = 1)
    concat_weekday.rename(columns={0 : 'Date'},inplace=True)
    concat_weekday.fillna(method = 'ffill',inplace=True)
    
''' clear date fields on website and enter end date variables '''
# assign date input variables if monday
if weekday == 0:
    # enter in start date for Friday and saturday
    start_time = driver.find_element_by_name('start')
    end_time = driver.find_element_by_name('end')
    # pull Friday data
    # click and enter info for beg and ending tike
    start_time.clear()

    # enter in prior week data
    start_time.send_keys(dates[0])
    start_time.send_keys(Keys.ENTER)

    # load page data for lxml
    time.sleep(10)
    html_source = driver.page_source




        
